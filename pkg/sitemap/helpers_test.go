// +build unit

package sitemap

import "strings"

func inputSample() []string {
	//returns all alphabet characters and spaces with repetitions as strings slice
	//uniq 27 chars
	return strings.Split("the quick brown fox jumps over the lazy dog", "")
}

var expectedAlphabet = []string{" ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
