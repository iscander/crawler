// +build unit

package sitemap

import (
	"runtime"
	"sort"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewSiteMap(t *testing.T) {
	siteMap := NewSiteMap(NewInMem(runtime.NumCPU())) //typical initialization for InMem backend
	assert.Implements(t, (*Queue)(nil), siteMap)
	assert.NotNil(t, siteMap.backend)
	assert.NotNil(t, siteMap.Queue())
	assert.NotNil(t, siteMap.List())
}

func TestQueueLoop(t *testing.T) {
	siteMap := NewSiteMap(NewInMem(runtime.NumCPU())) //typical initialization for InMem backend
	//fake queue consumer
	consumerDone := make(chan struct{})
	go func() {
		for {
			select {
			case <-consumerDone:
				return
			default:
				if _, ok := siteMap.Pop(); ok {
					//process values here
				}
			}
		}
	}()

	//Run background queue processing
	qwg := sync.WaitGroup{}
	qwg.Add(1)
	go func() {
		defer qwg.Done()
		siteMap.Run()
	}()

	//fill the data
	wg := sync.WaitGroup{}
	for _, str := range inputSample() {
		wg.Add(1)
		go func(sample string) {
			defer wg.Done()
			siteMap.Queue() <- sample
		}(str)
	}
	wg.Wait()
	siteMap.Done()
	qwg.Wait()
	close(consumerDone)
	got := make([]string, 0, 27)
	out := siteMap.List()
	for val := range out {
		got = append(got, val)
	}
	sort.Strings(got)
	assert.Equal(t, expectedAlphabet, got)
}
