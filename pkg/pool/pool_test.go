// +build unit

package pool

import (
	"runtime"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/iscander/crawler/pkg/sitemap"
)

func TestOneWorker(t *testing.T) {

	siteMap := sitemap.NewSiteMap(sitemap.NewInMem(runtime.NumCPU()))
	go func() { siteMap.Run() }()
	siteMap.Queue() <- "a"
	pool := NewPool(
		1,
		2*time.Second,
		siteMap.Queue(),
		siteMap.Pop,
		func() Worker { return &mockWorker{out: siteMap.Queue(), response: data} },
	)
	pool.Run()
	siteMap.Done()
	got := make([]string, 0, 224)
	for val := range siteMap.List() {
		got = append(got, val)
	}
	sort.Strings(got)
	assert.Equal(t, expectedUniq, got)
}

func TestMutlipleWorkers(t *testing.T) {
	siteMap := sitemap.NewSiteMap(sitemap.NewInMem(runtime.NumCPU()))
	concurency := runtime.NumCPU() + 3
	spliter := &slicer{slices: concurency}
	go func() { siteMap.Run() }()
	siteMap.Queue() <- "a"

	pool := NewPool(
		concurency,
		2*time.Second,
		siteMap.Queue(),
		siteMap.Pop,
		func() Worker { return &mockWorker{out: siteMap.Queue(), response: spliter.ranged()} },
	)
	pool.Run()
	siteMap.Done()

	got := make([]string, 0, 224)
	for val := range siteMap.List() {
		got = append(got, val)
	}
	sort.Strings(got)
	assert.Equal(t, expectedUniq, got)
}
