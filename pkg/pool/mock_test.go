// +build unit

package pool

import (
	"math/rand"
	"strings"
	"sync"
	"time"
)

func init() {
	data = strings.Split(lorem, " ")
	rand.Seed(time.Now().UnixNano())
}

var data []string

type mockWorker struct {
	response []string
	out      chan<- string
	counter  uint32
}

func (m *mockWorker) Get(string) {
	//println(atomic.AddUint32(&m.counter, 1))
	for _, link := range m.response {
		m.out <- link
	}
}

type slicer struct {
	slices  int
	counter int
	sync.Mutex
}

func (s *slicer) ranged() []string {
	var low, high int
	s.Lock()
	defer s.Unlock()
	if s.slices > 1 {
		sliceSize := rand.Intn((len(data)-s.counter)/s.slices - 1)
		low = int(s.counter)
		high = int(s.counter + sliceSize)
		s.counter = high
		s.slices--
	} else {
		low = int(s.counter)
		high = len(data)
	}
	return data[low:high]
}
