# CRAWLER

Simple web crawler for given a URL, it outputs a simple textual sitemap.
The crawler has limited to one subdomain - so when you start with **https://example.com/about**, it crawls all pages within example.com, but not follow external links, for example to _facebook.com_ or _subdomain.example.com_.
![Crawler schema](images/schema.png)
## Features

### Agenda

The list bellow represents feaures status for the project:

- [ ] incomplete task / TODO
- [x] completed 

### For the current version

- [x] Concurrent pages crawling, multiple crawlers run simultaneously
- [x] Use workers pool to limit number of crawlers
- [x] Arbitrary starting page
- [x] Collecting absolute and relative links on a page
- [x] Report the list of uniq URLs
- [x] Signal handling
- [x] In memory URL storage
- [x] Unit testing
- [x] Flexible and extendable applictaion design
- [x] Initial build environment - one only needs [docker](https://www.docker.com/) and [make](https://www.gnu.org/software/make/) util to build and test the project.
	- `make tests` to run unit tests 
	- `make checks` to run linters
	-  `make build` to build binaries under `./bin/` direcory:
		- linux i386, amd64 and arm7
		- windows 32 and 64 bits
		- MacOS
		 
- [] **TODO:** Add logging with log level support
- [] **TODO:** Add configuration parameters instead hardcoded values
	- Timeouts management
	- Concurency level
	- Force ignore HTTPS certificate validation
- [] **TODO:** Improve signal handling
- [] **TODO:** Improve page parsing
	- Support Relative links normalization (an url may contain links with **. ..** parts)
	- Support cross page link collecting
	- Support http and https links for the domain simultaneously
- [] **TODO:** Support SIGHUP to return statistic of run
- []  Add aditional (external) storage backends for collected URLs
	- Memcached
	- SQL
	- NoSQL
- [] Add integration tests
- [] Improve Makefile
- [] Hardening


## Example of the output

```
./bin/crawler.macos untagged master/45a9671 12/07/2019
Start crawling of "https://www.iana.org"...
run fetching procedure

:Stoped by SIGINT
```
...skipping **LONG** list of URLs...

```
https://www.iana.org/domains/root/db/fly.html
https://www.iana.org/reports/c.2.9.2.d/20160707-tjx
https://www.iana.org/reports/c.2.9.2.d/20140131-catering
https://www.iana.org/reports/2007/um-report-10jan2007.html
https://www.iana.org/domains/idn-tables/tables/accenture_ru_2.5.txt
https://www.iana.org/domains/idn-tables/tables/aco_zh-cn_4.0.txt
https://www.iana.org/domains/idn-tables/tables/author_de_2.1.txt
https://www.iana.org/domains/idn-tables/tables/kddi_ko_1.0.txt
https://www.iana.org/domains/idn-tables/tables/omega_kali_2.5.txt
https://www.iana.org/domains/idn-tables/tables/read_no_2.1.txt
https://www.iana.org/domains/idn-tables/tables/xn--q9jyb4c_jpan_1.0.txt
https://www.iana.org/about/performance/ietf-statistics/2009-04.pdf
https://www.iana.org/about/presentations/vegoda-sanjuan-ipv6-070624.pdf
https://www.iana.org/assignments/ianapowerstateset-mib/ianapowerstateset-mib.xhtml
https://www.iana.org/assignments/media-control-channel/media-control-channel.xhtml
https://www.iana.org/domains/idn-tables/tables/fairwinds_lydi_2.5.txt
https://www.iana.org/domains/idn-tables/tables/hitachi_es_1.0.txt
https://www.iana.org/domains/idn-tables/tables/hotmail_hani_2.5.txt
https://www.iana.org/domains/idn-tables/tables/ntt_ja_1.0.txt
https://www.iana.org/about/performance/ietf-statistics/2015-09.pdf
https://www.iana.org/about/performance/ietf-statistics/2011-07
https://www.iana.org/about/performance/ietf-statistics/2010-07
https://www.iana.org/domains/root/db/nikon.html
https://www.iana.org/domains/idn-tables/tables/hair_uk_2.5.txt
https://www.iana.org/domains/root/db/audi.html
https://www.iana.org/domains/root/db/xn--otu796d.html
https://www.iana.org/reports/c.2.9.2.d/20140811-bnpparibas
https://www.iana.org/domains/idn-tables/tables/aarp_hano_2.5.txt

Done
```